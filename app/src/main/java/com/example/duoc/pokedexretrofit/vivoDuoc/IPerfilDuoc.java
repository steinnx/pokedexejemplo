package com.example.duoc.pokedexretrofit.vivoDuoc;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
/**
 * Created by SteinnxLabs on 18-06-2017.
 */

public interface IPerfilDuoc {
    @GET("loginAlumno")
    Call<ResponseLogin> login(@Query("usuario") String user,
                              @Query("password") String pass);

}
