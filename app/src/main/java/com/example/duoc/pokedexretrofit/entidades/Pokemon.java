package com.example.duoc.pokedexretrofit.entidades;

/**
 * Created by DUOC on 30-06-2017.
 */

public class Pokemon {
    //importante en el get del number cambia la url para
    //     poder recuperar el .jpg del api rest dependiendo de la api rest se hace o no esto xd
    private int number;
    private String name;
    private String url;

    public int getNumber() {
        String[] urlPartes =url.split("/");
        return Integer.parseInt(urlPartes[urlPartes.length-1]);
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
