package com.example.duoc.pokedexretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import com.example.duoc.pokedexretrofit.entidades.Pokemon;
import com.example.duoc.pokedexretrofit.entidades.PokemonAdapter;
import com.example.duoc.pokedexretrofit.entidades.PokemonRespuesta;
import com.example.duoc.pokedexretrofit.pokeApi.PokeApiServices;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ListarActivity extends AppCompatActivity {

    private TextView tv_usuario,tv_rut;

    private static final String TAG="POKEDEX";
    private Retrofit retrofit;
    private RecyclerView recyclerView;
    private PokemonAdapter pokemonAdapter;

    private int offset;

    private boolean aptoParaCargar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listar);
        //obtener datos del intent del login
        String var_inten_nombre=getIntent().getStringExtra("usuario");
        String var_inten_rut=getIntent().getStringExtra("rut");
        //seteo los textview del layout con los datos del usuario
        tv_usuario= (TextView) findViewById(R.id.tv_nombre);
        tv_rut= (TextView) findViewById(R.id.tv_rut);
        tv_usuario.setText(var_inten_nombre);
        tv_rut.setText(var_inten_rut);
        //cargo reciclerview
        recyclerView = (RecyclerView) findViewById(R.id.rv_listar);
        pokemonAdapter = new PokemonAdapter(this);
        recyclerView.setAdapter(pokemonAdapter);
        recyclerView.setHasFixedSize(true);
        //Cargo grilla de 3 columanas para el reciclerview
        final GridLayoutManager layoutManager = new GridLayoutManager(this,3);
        recyclerView.setLayoutManager(layoutManager);

        //ahora para saber listar el recyclerview de 20 en 20
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //hacemos serie de preguntas si esta haciendo scroll hacia abajo
                if (dy>0){
                    int visibleItemCount = layoutManager.getChildCount();
                    int totalItemCount = layoutManager.getItemCount();
                    int pastVisibleItems = layoutManager.findFirstVisibleItemPosition();
                    //ademas crearemos un boolean que permita seguir haciendo scroll la declararemos globalmente
                    if (aptoParaCargar){
                        //valida si puede seguir haciendo scroll
                        if ((visibleItemCount+pastVisibleItems)>=totalItemCount){
                            Log.e(TAG,"Llegamos al final");
                            aptoParaCargar=false;
                            offset+=20;
                            obtenerDatos(offset);
                        }
                        //para que no haya errores seteamos la variable aptoParaCargar en los puntos necesarios en el
                        //onCreate y en el onResponse
                    }
                }
            }
        });


         //instancio retrofit.. y me cuelgo de la API REST
        retrofit = new Retrofit.Builder()
                .baseUrl("http://pokeapi.co/api/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //
        aptoParaCargar=true;
        offset=0;
        obtenerDatos(offset);


    }

    private void obtenerDatos(int offset) {
        PokeApiServices apiServices = retrofit.create(PokeApiServices.class);
        Call<PokemonRespuesta> pokemonRespuestaCall=apiServices.obtenerListaPokemones(20,offset);

        pokemonRespuestaCall.enqueue(new Callback<PokemonRespuesta>() {
            @Override
            public void onResponse(Call<PokemonRespuesta> call, Response<PokemonRespuesta> response) {

                aptoParaCargar=true;
                if (response.isSuccessful()){
                    //si esta bn la solicitud le devuelvo el gson mediante response.body
                    PokemonRespuesta pokemonRespuesta = response.body();
                    //traspaso los objetos a un arraylist de pokemones
                    ArrayList<Pokemon> pokemones = pokemonRespuesta.getResults();

                    //Listar en el recyclerview
                    pokemonAdapter.adiccionarListaPokemon(pokemones);


                    //ejemplo
                    /*for(int i=0;i<pokemones.size();i++){
                        Pokemon p = pokemones.get(i);
                        Log.i(TAG,"Pokemon: "+p.getName());
                    }*/

                }else{
                    Log.e(TAG,"onResponese: "+response.errorBody());
                }
            }

            @Override
            public void onFailure(Call<PokemonRespuesta> call, Throwable t) {
                aptoParaCargar=true;
                Log.e(TAG,"onFailure: "+t.getMessage());
            }
        });
    }
}
