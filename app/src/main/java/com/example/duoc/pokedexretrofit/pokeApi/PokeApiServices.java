package com.example.duoc.pokedexretrofit.pokeApi;

import com.example.duoc.pokedexretrofit.entidades.PokemonRespuesta;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by DUOC on 30-06-2017.
 */
//paso 2 crear interfaz
public interface PokeApiServices {
    //anotacion get.. de parametro le introducimos el objeto a llamar
    //SOLO LISTA LOS PRIMEROS 20
    /*@GET("pokemon")
    Call<PokemonRespuesta> obtenerListaPokemones();*/

    //AHORA LISTAREMOS DE 20 EN 20 CON LIMIT Y OFFSET
    //IMPORTANTE para que retrofit sepa que son parametros de la URL debemos anteponer @Query y en comillas el dato que piden
    @GET("pokemon")
    Call<PokemonRespuesta> obtenerListaPokemones(@Query("limit") int limit,@Query("offset") int offset);
}
