package com.example.duoc.pokedexretrofit.entidades;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.example.duoc.pokedexretrofit.R;

import java.util.ArrayList;

/**
 * Created by SteinnxLabs on 30-06-2017.
 */

public class PokemonAdapter extends RecyclerView.Adapter<PokemonAdapter.PokemonViewHolder> {

    private ArrayList<Pokemon> dataSource;
    private Context context;

    public PokemonAdapter(Context context){
        this.context= context;
        dataSource = new ArrayList<>();
    }

    @Override
    public PokemonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pokemon,parent,false);
        return new PokemonViewHolder(view);
    }

    //aqui se setean lo componentes del item_pokemon uno por uno
    @Override
    public void onBindViewHolder(PokemonViewHolder holder, int position) {
        Pokemon p = dataSource.get(position);
        holder.tv_pokemon.setText(p.getName());

        //ocupamos glide para mostrar imagen se puede con picasso pero el ejemplo lo hicieron con glide antes añadir a gradle
        //OJO IMPORTANTE COLOCAMOS LA URL DE LA IMAGEN Y EN /POKEMON/[OBJETO.NUMERO].PNG
        // asi buscara el png del objeto mediante su numero
        Glide.with(context)
                .load("http://pokeapi.co/media/sprites/pokemon/"+p.getNumber()+".png")
                .centerCrop()
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.im_pokemon);
    }

    @Override
    public int getItemCount() {
        return dataSource.size();
    }

    public void adiccionarListaPokemon(ArrayList<Pokemon> pokemones) {
        dataSource.addAll(pokemones);
        //actualizar reciclerview en pantalla
        notifyDataSetChanged();
    }

    //creo clase viewholder
    public class PokemonViewHolder extends RecyclerView.ViewHolder{

        private ImageView im_pokemon;
        private TextView tv_pokemon;
        public PokemonViewHolder(View itemView) {
            super(itemView);
            im_pokemon=(ImageView) itemView.findViewById(R.id.iv_pokemon);
            tv_pokemon=(TextView) itemView.findViewById(R.id.tv_pokemon);
        }
    }
}
