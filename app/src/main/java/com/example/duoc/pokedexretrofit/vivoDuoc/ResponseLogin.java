package com.example.duoc.pokedexretrofit.vivoDuoc;

import com.google.gson.annotations.SerializedName;

/**
 * Created by SteinnxLabs on 18-06-2017.
 */

public class ResponseLogin {
    @SerializedName("data")
    private Perfil perfil;

    public ResponseLogin(Perfil perfil) {
        this.perfil = perfil;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
}
