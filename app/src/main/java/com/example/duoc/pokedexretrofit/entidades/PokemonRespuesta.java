package com.example.duoc.pokedexretrofit.entidades;

import java.util.ArrayList;

/**
 * Created by DUOC on 30-06-2017.
 */

public class PokemonRespuesta {

    private ArrayList<Pokemon> results;

    public ArrayList<Pokemon> getResults() {
        return results;
    }

    public void setResults(ArrayList<Pokemon> results) {
        this.results = results;
    }
}
