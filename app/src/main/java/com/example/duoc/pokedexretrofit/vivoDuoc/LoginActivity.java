package com.example.duoc.pokedexretrofit.vivoDuoc;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.duoc.pokedexretrofit.ListarActivity;
import com.example.duoc.pokedexretrofit.R;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import okhttp3.*;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener{

    //
    private EditText etUsuario;
    private EditText etContraseña;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        etUsuario = (EditText) findViewById(R.id.etUsuarioLogin);
        etContraseña = (EditText) findViewById(R.id.etContraseñaLogin);

        btnLogin =(Button) findViewById(R.id.btnIngresar);

        btnLogin.setOnClickListener(this);

    }


    //LOGIN DUOC
    private void doLogin() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor).build();
        Gson gson = new GsonBuilder()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://vivo.duoc.cl/VivoMobileServer/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();

        IPerfilDuoc service = retrofit.create(IPerfilDuoc.class);

        Call<ResponseLogin> repos = service.login(etUsuario.getText().toString(),
                etContraseña.getText().toString());
        repos.enqueue(new Callback<ResponseLogin>() {
            @Override
            public void onResponse(Call<ResponseLogin> call, Response<ResponseLogin> response) {
                if (response.code() == 200) {
                    ResponseLogin values = response.body();
                    //Toast.makeText(LoginActivity.this, "Nombre: " + values.getPerfil().getNombreCompleto(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(LoginActivity.this,ListarActivity.class);
                    intent.putExtra("usuario","Usuario Duoc: "+values.getPerfil().getNombreCompleto());
                    intent.putExtra("rut","Rut: "+values.getPerfil().getRut());
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<ResponseLogin> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId()==R.id.btnIngresar){
            //Ingresar();
            doLogin();
        }
    }
}
